#bot.py
import os
import discord
from discord.ext import commands

from dotenv import load_dotenv

load_dotenv()
token = os.getenv('DISCORD_TOKEN')

bot = commands.Bot(command_prefix='?')

client = discord.Client()

@bot.event
async def on_ready():
    print(f"{bot.user} connected to discord!")

@bot.event
async def on_member_join(member):
    await member.create_dm()
    await member.dm_channel_send(
        f'Hi {member.name}, welcome to The Aquos Pack! Please read #rules!\n'
        )

#Moderation Commands
@bot.command()
@commands.has_permissions(kick_members=True)
async def kick(ctx, member : discord.Member, *, reason = None):
    await member.kick(reason=reason)

@bot.command()
@commands.has_permissions(ban_members=True)
async def ban(ctx, member : discord.Member, *, reason = None):
    await member.ban(reason=Reason)

#Fun Commands
@bot.command(name='hug')
async def hug(ctx, other:str):
    aquos_hug = (
        f'{other} has been given a hug by {ctx.author.mention}!\n'
        )
    response = aquos_hug
    await ctx.send(response)
@bot.command(name="hello")
async def hello(ctx):
    aquos_hello = (
        f'{ctx.author.mention} waves hello!\n'
    )
    response = aquos_hello
    await ctx.send(response)
@bot.command(name='lurk')
async def lurk(ctx):
    aquos_lurk = (
        f'{ctx.author.mention} is lurking in the shadows!\n'
    )

    response = aquos_lurk
    await ctx.send(response)

@bot.command(name='bap')
async def bap(ctx, other:str):
    aquos_bap = (
        f'{ctx.author.mention} has bapped {other}!\n'
        )
    response = aquos_bap
    await ctx.send(response)


bot.run(token)
