# Import Discord
import discord
import os


# Import Commands and Environment
from discord.ext import commands
from dotenv import load_dotenv


# Load Environment and Token
load_dotenv()
token = os.getenv("DISCORD_TOKEN")


# Prefix and Discord Client
bot = commands.Bot(command_prefix="//")
client = discord.Client()


# Login Event
@bot.event
async def on_ready():
    print(f"{bot.user} has connected to Discord")
    await bot.change_presence(status=discord.Status.online, activity=discord.Streaming(name="CalebOWolf", url="https://twitch.tv/calebowolf", platform="Twitch", details="Wolf Howl", game="Wolf Howl", twitch_name="CalebOWolf"))


# Moderation Commands
@bot.command()
@commands.has_permissions(kick_members=True)
async def kick(ctx, member: discord.Member, *, reason=None):
    await member.kick(reason=reason)


@bot.command()
@commands.has_permissions(ban_members=True)
async def ban(ctx, member: discord.Member, *, reason=None):
    await member.ban(reason=reason)


# Intents
intents = discord.Intents(messages=True, guilds=True)
intents.typing = True
intents.presences = True



# Bot Commands

# Awoo
@bot.command(name="awoo")
async def awoo(ctx):
    wolf_awoo = (
        f"AWOOOO \n"
    )
    response = wolf_awoo
    await ctx.send(response)


# Bork
@bot.command(name="bork")
async def bork(ctx):
    wolf_bork = (
        f"Bork \n"
    )
    response = wolf_bork
    await ctx.send(response)


# Bark
@bot.command(name="bark")
async def bark(ctx):
    wolf_bark = (
        f"Bark \n"
    )
    response = wolf_bark
    await ctx.send(response)


# Boop
@bot.command(name="boop")
async def boop(ctx, other: str):
    wolf_boop = (
        f"{ctx.author.mention} boops {other} \n"
    )
    response = wolf_boop
    await ctx.send(response)


# Bap
@bot.command(name="bap")
async def bap(ctx, other: str):
    wolf_bap = (
        f"{ctx.author.mention} baps {other} \n"
    )
    response = wolf_bap
    await ctx.send(response)

# Blep
@bot.command(name="blep")
async def blep(ctx, other: str):
    wolf_blep = (
        f"{ctx.author.mention} Bleps \n"
    )
    response = wolf_blep
    await ctx.send(response)


# Cuddle
@bot.command(name="cuddle")
async def cuddle(ctx, other: str):
    wolf_cuddle = (
        f"{ctx.author.mention} cuddles {other} \n"
    )
    response = wolf_cuddle
    await ctx.send(response)


# Cutie
@bot.command(name="cutie")
async def cutie(ctx, other: str):
    wolf_cutie = (
        f"{ctx.author.mention} is a cutie! \n"
    )
    response = wolf_cutie
    await ctx.send(response)

# Flop
@bot.command(name="flop")
async def flop(ctx, other: str):
    wolf_flop = (
        f"{ctx.author.mention} Flops! \n"
    )
    response = wolf_flop
    await ctx.send(response)


# Furpile
@bot.command(name="furpile")
async def furpile(ctx):
    wolf_furpile = (
        f"Furpile! \n"
    )
    response = wolf_furpile
    await ctx.send(response)

# Gay
@bot.command(name="gay")
async def gay(ctx):
    wolf_gay = (
        f"GAYYYY \n"
    )
    response = wolf_gay
    await ctx.send(response)


# Hello
@bot.command(name="hello")
async def hello(ctx):
    wolf_hello = (
        f"Hello There! \n"
    )
    response = wolf_hello
    await ctx.send(response)


# Hold
@bot.command(name="hold")
async def hold(ctx, other: str):
    wolf_hold = (
        f"{ctx.author.mention} holds {other} \n"
    )
    response = wolf_hold
    await ctx.send(response)


# Hug
@bot.command(name="hug")
async def hug(ctx, other: str):
    wolf_hug = (
        f"{ctx.author.mention} hugs {other} \n"
    )
    response = wolf_hug
    await ctx.send(response)


# Huff
@bot.command(name="huff")
async def huff(ctx):
    wolf_huff = (
        f"Huff! \n"
    )
    response = wolf_huff
    await ctx.send(response)


# Kiss
@bot.command(name="kiss")
async def kiss(ctx, other: str):
    wolf_kiss = (
        f"{ctx.author.mention} kisses {other} \n"
        f"<:wolflove:786751798652043274> <:wolflove2:787096391562166293> \n"

    )
    response = wolf_kiss
    await ctx.send(response)


# Kill
@bot.command(name="kill")
async def kill(ctx, other: str):
    wolf_kill = (
        f"{ctx.author.mention} kills {other} \n"
    )
    response = wolf_kill
    await ctx.send(response)


# Lick
@bot.command(name="lick")
async def lick(ctx, other: str):
    wolf_lick = (
        f"{ctx.author.mention} licks {other} \n"
    )
    response = wolf_lick
    await ctx.send(response)


# Lurk
@bot.command(name="lurk")
async def lurk(ctx, other: str):
    wolf_lurk = (
        f"{ctx.author.mention} Lurks \n"
    )
    response = wolf_lurk
    await ctx.send(response)


# Nap
@bot.command(name="nap")
async def nap(ctx):
    wolf_nap = (
        f"Nap! \n"
    )
    response = wolf_nap
    await ctx.send(response)

# Nuzzle
@bot.command(name="nuzzle")
async def nuzzle(ctx, other: str):
    wolf_nuzzle = (
        f"{ctx.author.mention} nuzzles {other} \n"
    )
    response = wolf_nuzzle
    await ctx.send(response)


# Pet
@bot.command(name="pet")
async def pet(ctx, other: str):
    wolf_pet = (
        f"{ctx.author.mention} pets {other} \n"
    )
    response = wolf_pet
    await ctx.send(response)


# Poke
@bot.command(name="poke")
async def poke(ctx, other: str):
    wolf_poke = (
        f"{ctx.author.mention} pokes {other} \n"
    )
    response = wolf_poke
    await ctx.send(response)


# Pounce
@bot.command(name="pounce")
async def pounce(ctx, other: str):
    wolf_pounce = (
        f"{ctx.author.mention} pounces on {other} \n"
    )
    response = wolf_pounce
    await ctx.send(response)


# Slap
@bot.command(name="slap")
async def slap(ctx, other: str):
    wolf_slap = (
        f"{ctx.author.mention} slaps {other} \n"
    )
    response = wolf_slap
    await ctx.send(response)


# Sniff
@bot.command(name="sniff")
async def sniff(ctx, other: str):
    wolf_sniff = (
        f"{ctx.author.mention} sniffs {other} \n"
    )
    response = wolf_sniff
    await ctx.send(response)


# Wag
@bot.command(name="wag")
async def wag(ctx, other: str):
    wolf_wag = (
        f"{ctx.author.mention} wags! \n"
    )
    response = wolf_wag
    await ctx.send(response)


# whosagoodboi
@bot.command(name="whosagoodboi")
async def whosagoodboi(ctx, other: str):
    wolf_whosagoodboi = (
        f"{ctx.author.mention} Whos a good Boi!? \n"
    )
    response = wolf_whosagoodboi
    await ctx.send(response)


# Luca
@bot.command(name="luca")
async def luca(ctx):
    wolf_luca = (
        f"https://www.dropbox.com/s/t6g6ofssnjg7m5i/image0-2.png?dl=1 \n"
    )
    response = wolf_luca
    await ctx.send(response)


# Love
@bot.command(name='love')
async def love(ctx):
    wolf_love = (
        f' <:wolflove:786751798652043274> <:wolflove2:787096391562166293>  \n'
    )
    response = wolf_love
    await ctx.send(response)


# Test
@bot.command(name='test')
async def test(ctx):
    wolf_test = (
        f' Test! \n'
    )
    response = wolf_test
    await ctx.send(response)


# Run Wolf Howl
bot.run(token)
